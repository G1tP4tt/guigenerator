package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import org.hamcrest.Matchers;
import org.junit.Test;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Dataset;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.Fenster;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.testClasses.ClassWithThreePublicFields;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.WindowCreator;

public class WindowCreatorTest {

	@Test
	public void erzeuge() throws Exception {
		Dataset datensatz = mock(Dataset.class);
		ClassWithThreePublicFields innermock = new ClassWithThreePublicFields();
		when(datensatz.getInstance()).thenReturn(innermock);
		WindowCreator fensterErzeuger = new WindowCreator();

		Fenster erzeugtesFenster = fensterErzeuger.create(datensatz);

		assertThat(erzeugtesFenster.getId(), Matchers.is("ClassWithThreePublicFields"));

	}

}

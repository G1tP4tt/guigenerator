package de.dhbw.vs.jprakt.guigenerator.gruppe3;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.StringElement;

public class MockGenerator {
	private static String DEFAULT_NAME = "Hugo";

	public static StringElement mockTextfeldMitLabel( String fieldName) {
		StringElement textfeld = mock(StringElement.class);
		when(textfeld.getLabel()).thenReturn(fieldName);
		Field mockField = mockField(fieldName);
		when(textfeld.getAssociatedField()).thenReturn(mockField);
		return textfeld;
	}

	public static StringElement mockTextfeldMitLabel() {
		return mockTextfeldMitLabel(DEFAULT_NAME);
	}

	public static Field mockField(String name) {
		Field mock = mock(Field.class);
		when(mock.getName()).thenReturn(name);
		return mock;
	}
}
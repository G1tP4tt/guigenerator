package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleTextfieldBindingTest {

	@Test
	public void IsValidInteger_True_ForStringWithLeadingZero() throws Exception {
		isValidDouble_ForValue(true, "-3.1");
	}

	public void isValidDouble_ForValue(boolean expected, String value) {

		// action, class under test
		boolean validInteger = DoubleTextfieldBinding.isValidDouble(value);

		assertThat(validInteger, Matchers.is(expected));
	}

	@Test
	public void IsValidInteger_False_ForStringThatContainsNonNumberChars() throws Exception {
		isValidDouble_ForValue(false, "123.456bb");
	}

	@Test
	public void isValidInteger_False_ForEmptyString() throws Exception {
		isValidDouble_ForValue(false, "");
	}
	
	@Test
	public void isValidInteger_True_ForZero() throws Exception {
		isValidDouble_ForValue(true, "0.0");
	}

}

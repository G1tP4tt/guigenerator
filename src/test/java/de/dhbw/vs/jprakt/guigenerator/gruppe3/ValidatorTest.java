package de.dhbw.vs.jprakt.guigenerator.gruppe3;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import de.dhbw.vs.jprakt.guigenerator.gruppe3.testClasses.ClassWithThreePublicFields;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.validation.Validator;

public class ValidatorTest {
	private Validator classUnderTest;
	
	@Before
	public void init(){

		classUnderTest = new Validator();

	}
	
	private void checkPattern(String testableObject, String pattern, boolean expectedResult) {
		Boolean result = classUnderTest.matchesPattern(testableObject, pattern);

		assertThat(result, Matchers.is(expectedResult));
	}
	
	private void testSizeIsBetween(String testableObject, boolean expectedResult) {
		int minsize = 2;
		int maxsize = 10;
		boolean result = classUnderTest.sizeIsBetween(testableObject, minsize, maxsize);

		assertThat(result, Matchers.is(expectedResult));
	}

	@Test
	public void notNull_ReturnsTrueforNewTestClaz() throws Exception {

		boolean result = classUnderTest.notNull(new ClassWithThreePublicFields());

		assertThat(result, Matchers.is(true));
	}

	@Test
	public void checkPattern_ReturnsTrueforMatchingString() throws Exception {

		checkPattern("test@email.org", "\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", true);

	}


	
	@Test
	public void checkPattern_ReturnsFalseforNonMatchingString() throws Exception {

		checkPattern("testemail.org", ".*@.*\\.org", false);

	}

	@Test
	public void sizeIsBetween_LongerStringReturnsFalse() throws Exception {
		testSizeIsBetween("LongerThanAl", false);

	}
	
	@Test
	public void sizeIsBetween_ShorterStringReturnsFalse() throws Exception {
		testSizeIsBetween("s", false);

	}
	
	@Test
	public void sizeIsBetween_InBetweenStringReturnsTrue() throws Exception {
		testSizeIsBetween("correctlen", true);

	}
	
}

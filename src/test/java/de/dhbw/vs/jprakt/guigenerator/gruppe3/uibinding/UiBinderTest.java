package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.ClassScanner;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Dataset;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.testClasses.ClassWithThreePublicFields;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.BindingElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield.StringTextfieldBinding;

public class UiBinderTest {
	@Test
	public void testName() throws Exception {
		//Datensatz erstellen
		Dataset dataset = ClassScanner.createDataset(new ClassWithThreePublicFields());
		
		
		//Bindings erstellen
		
		List<BindingElement> uiBindings=UiBinder.create(dataset);
		
		//fertig
		assertThat(uiBindings, Matchers.hasSize(3));
		assertEquals(uiBindings.get(0).getClass(), StringTextfieldBinding.class);
		//Checken ob Binding:
		//korrekter Typ
		//korrekte Anzahl
		
		
		
	}
}

package de.dhbw.vs.jprakt.guigenerator.gruppe3.validation;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hamcrest.Matchers;
import org.junit.Test;

public class ValidationAnnotationUnmarshallerTest {

	@Test
	public void unmarshallAnnotation() throws Exception {
		Annotation annotation = mock(NotNull.class);
		Validator validator = mock(Validator.class);
		Object toValidate = null;
		AnnotationValidator marshaller = new AnnotationValidator(validator);
		
		marshaller.validate(annotation, toValidate );
		
		verify(validator, atLeast(1)).notNull(null);
	}
	
	@Test
	public void validateNotNullAnnotationForNullResultContainsError() throws Exception {
		Annotation annotation = mock(NotNull.class);
		Validator validator = new Validator();
		Object toValidate = null;
		AnnotationValidator marshaller = new AnnotationValidator(validator);
		
		SingleResult result = marshaller.validate(annotation, toValidate ).get();
		
		assertThat(result.isValid() , Matchers.is(false));
	}
	
	@Test
	public void validateNotNullAnnotationForNullResultContainsErrorHasMessage() throws Exception {
		NotNull annotation = mock(NotNull.class);
		when(annotation.message()).thenReturn("Das Attribut vorname muss gesetzt sein.");
		Validator validator = new Validator();
		Object toValidate = null;
		AnnotationValidator marshaller = new AnnotationValidator(validator);
		
		SingleResult result = marshaller.validate(annotation, toValidate ).get();
		
		assertThat(result.message() , Matchers.is("Das Attribut vorname muss gesetzt sein."));
	}
	//TODO Test für Size und Pattern
	
	@Test
	public void validateListOfAnnotations() throws Exception {
		
		List<Annotation>annotations=new ArrayList<Annotation>();
		annotations.add(mock(NotNull.class));
		annotations.add(mock(Pattern.class));
		annotations.add(mock(Size.class));
		annotations.add(mock(Annotation.class));
		
		
		Validator validator = new Validator();
		Object toValidate = null;
		AnnotationValidator marshaller = new AnnotationValidator(validator);
		
		List<SingleResult> result = marshaller.validate(annotations, toValidate );
		
		assertThat(result , Matchers.hasSize(3));
	}
}

package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield;

import static org.junit.Assert.*;
import org.hamcrest.Matchers;
import org.junit.Test;

import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield.IntegerTextfieldBinding;

public class IntegerTextfieldBindingTest {

	@Test
	public void IsValidInteger_True_ForStringWithLeadingZero() throws Exception {
		isValidInteger_ForValue(true, "-3");
	}

	public void isValidInteger_ForValue(boolean expected, String value) {

		// action, class under test
		boolean validInteger = IntegerTextfieldBinding.isValidInteger(value);

		// assert
		assertThat(validInteger, Matchers.is(expected));
	}

	@Test
	public void IsValidInteger_False_ForStringThatContainsNonNumberChars() throws Exception {
		isValidInteger_ForValue(false, "123456bb");
	}

	@Test
	public void isValidInteger_False_ForEmptyString() throws Exception {
		isValidInteger_ForValue(false, "");

	}
	
	@Test
	public void isValidInteger_True_ForZero() throws Exception {
		isValidInteger_ForValue(true, "0");
	}

}

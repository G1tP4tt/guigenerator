package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield;


import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;

import org.hamcrest.Matchers;
import org.junit.Test;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.StringElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.BindingElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield.StringTextfieldBinding;

public class StringTextfieldBindingTest {

	@Test
	public void transformToGUITest() {
		//Set Up
		UiElement<String> textfieldMock = mock(UiElement.class);
		Field textfieldFieldMock = mock(Field.class);
		when(textfieldMock.getInstanceValue()).thenReturn("Initialer Wert");
		when(textfieldMock.getAssociatedField()).thenReturn(textfieldFieldMock);
		when(textfieldFieldMock.getName()).thenReturn("Name");
		
		UiElementRepresentation<String> representation = null;
		BindingElement<String, String> uiBinding = new StringTextfieldBinding().init(textfieldMock);;
		
		//Action
		representation = uiBinding.getRepresentation();
		
		//Assert
		assertThat(representation.getValue(), Matchers.is("Initialer Wert"));
	}
	
	@Test
	public void writeBack() throws Exception {
		//Set Up
		Field fieldMock = mock(Field.class);
		when(fieldMock.getName()).thenReturn("Name");
		UiElement<String> textfield = new StringElement("Initialer Wert", fieldMock);
		UiElementRepresentation<String> representation = mock(UiElementRepresentation.class);
		when(representation.getValue()).thenReturn("Geänderter Wert"); //Mocks a changed Content of Textfield in GUI
		
		//Action
		UiElement<String> changedTextfield = new StringTextfieldBinding().writeBack(representation, textfield);
		String changedTextfieldValue = changedTextfield.getInstanceValue();
		
		//Assert
		assertThat(changedTextfieldValue, Matchers.is("Geänderter Wert"));
	}
	

}
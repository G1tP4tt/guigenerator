package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * 
 * This is the documentation of the class PanelPie. This class will create a
 * JPanel including the pieChart depending on the input data given in a hashMap.
 * 
 * @author Vera Gögelein
 * @author Janine Salomon
 *
 */
public class PieChartPie extends JPanel implements Runnable {
	/**
	 * Array with different colors for filling purposes of the pieChart.
	 */
	private static Color[] ColorPies = { Color.yellow, Color.orange, Color.pink, Color.magenta, Color.red,
			Color.lightGray, Color.green, Color.gray, Color.darkGray, Color.cyan, Color.blue, Color.black };
	/**
	 * Deklaration of the ArrayList values.
	 */
	private ArrayList<Number> values;
	/**
	 * Deklaration of the angles.
	 */
	private double startAngle = 0.0;
	private double arcAngle = 0.0;
	/**
	 * Deklaration of degree and sumValue.
	 */
	private double degree = 0.0;
	private int sumValue = 0;
	/**
	 * Deklaration for setting the size of the pieChart and the start.
	 */
	private int sizePie = 0;
	private int startPie = 0;

	/**
	 * Deklaration of ArrayLists for the startPoints and angles.
	 */
	ArrayList<Integer> startPoints = new ArrayList<Integer>();
	ArrayList<Integer> angles = new ArrayList<Integer>();

	/**
	 * Deklaration of the array workAngles stores the actual angles of the
	 * wedges..
	 */
	private int[] workAngles;

	/**
	 * Constructor for the class PanelPie.
	 * 
	 * @param values
	 *            ArrayList with values.
	 * @param size
	 *            Size of the pieChart.
	 */
	public PieChartPie(ArrayList<Number> values, int heightPanel, int widthPanel) {
		this.values = values;
		this.setSize(widthPanel, heightPanel);
		drawPie();
	}

	/**
	 * The method paintComponent is drawing the pieChart.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < workAngles.length; i++) {
			// Setting colors for the rectangles of the legend. If there are
			// more than 12 input
			// data, the colors will be set sometimes twice.
			g.setColor(ColorPies[i % workAngles.length]);
			g.fillArc(startPie, startPie, sizePie, sizePie, startPoints.get(i), workAngles[i]);
		}
	}

	/**
	 * This method calculates the sum of all the values in the values array and
	 * the value of one degree of the Chart.
	 */
	public void drawPie() {
		if (this.getHeight() <= this.getWidth()) {
			sizePie = (int) (this.getHeight() * 0.9);
		} else {
			sizePie = this.getWidth();
		}
		startPie = (int) Math.round(sizePie * 0.05);
		workAngles = new int[values.size()];
		ArrayList<Double> arcAngles = new ArrayList<Double>();
		ArrayList<Double> startAngles = new ArrayList<Double>();
		// Calculating the size of an angle depended on the sum of all values.
		for (int i = 0; i < values.size(); i++) {
			sumValue = sumValue + values.get(i).intValue();
		}
		degree = 360.00 / (double) sumValue;
		for (int i = 0; i < values.size(); i++) {
			startAngle = (int) (startAngle + arcAngle);
			startAngles.add(startAngle);
			arcAngle = Math.round(degree * values.get(i).doubleValue());
			arcAngles.add(arcAngle);
			// Creating ArrayList to store the startPoints and the size of the
			// wedges
			int start;
			int angle;
			if (i == 0) {
				// Calculating the first angle.
				start = startAngles.get(i).intValue();
				angle = arcAngles.get(i).intValue();
			} else if (i == values.size()) {
				// Calculating the angles between the first and the last one.
				start = startAngles.get(i - 1).intValue() + arcAngles.get(i - 1).intValue();
				angle = 360 - (startAngles.get(i - 1).intValue() + arcAngles.get(i - 1).intValue());
			} else {
				// Calculating the last angle.
				start = startAngles.get(i - 1).intValue() + arcAngles.get(i - 1).intValue();
				angle = arcAngles.get(i).intValue();
			}
			// Adding the startPosition and the angle of the actual wedge to the
			// ArrayLists.
			startPoints.add(start);
			angles.add(angle);
		}
	}

	/**
	 * Run method needed for threads.
	 */
	@Override
	public void run() {

		boolean change = true;
		while (change) {
			change = false;
			for (int i = 0; i < values.size(); i++) {
				if (workAngles[i] < angles.get(i)) {
					change = true;
					workAngles[i]++;
				}
			}
			repaint();
			try {
				Thread.sleep(20);
			} catch (Exception ex) {
			}
		}
	}

	/**
	 * The method repaintSizing is setting the new size of the JPanel and
	 * initiate a repaint of the new pieChart.
	 * 
	 * @param heightPanel
	 *            Height of the JPanel
	 * @param widthPanel
	 *            Width of the JPanel
	 */
	public void repaintSizing(int heightP, int widthP) {
		this.removeAll();
		this.setSize(widthP, heightP);
		drawPie();
		repaint();
	}
}
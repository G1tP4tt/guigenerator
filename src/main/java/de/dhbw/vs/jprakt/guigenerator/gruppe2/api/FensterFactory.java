package de.dhbw.vs.jprakt.guigenerator.gruppe2.api;

/*
 * Factory zum Erstellen von Fenstern
 */
public interface FensterFactory {

	Fenster create(String name);

}

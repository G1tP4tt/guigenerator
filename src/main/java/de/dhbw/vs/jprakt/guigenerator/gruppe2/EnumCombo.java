package de.dhbw.vs.jprakt.guigenerator.gruppe2;


import java.awt.GridLayout;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

public class EnumCombo extends JPanel implements UiElementRepresentation<Integer> {
	
	private JComboBox<String> content;
	private JLabel labelId;
	
	public EnumCombo(List<String> contentProvider, int selection, String id) { 
		this.labelId = new JLabel(id);
		this.content = new JComboBox<String>();
		//Adds every element of List contentProvider as selctionoption to combobox
		for(String s : contentProvider) {
			this.content.addItem(s);
		}
		//sets the preselected Item
		this.content.setSelectedIndex(selection);
		addComponent();
	}

	@Override
	public String getId() {
		return this.labelId.getText();
	}

	@Override
	public Integer getValue() {
		return content.getSelectedIndex();
	}

	@Override
	public JComponent getComponent() {
		return this;
	}
	
	/**
	 * Adds the label and the combobox to the panel with gridlayout 1,2
	 */
	private void addComponent() {
		this.setLayout(new GridLayout(1, 2));
		this.add(this.labelId);
		this.add(this.content);
		
	}

	@Override
	public void highlight() {
		
	}

}

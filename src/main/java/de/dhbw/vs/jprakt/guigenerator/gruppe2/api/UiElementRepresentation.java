package de.dhbw.vs.jprakt.guigenerator.gruppe2.api;

import javax.swing.JComponent;


public interface UiElementRepresentation<T> {

	/**
	 * Der Titel dient als Id eines UiElementRepresentation-Objektes.
	 * @return
	 */
	String getId();

	T getValue();

	/**
	 * the JComponent is only Part of the UiElement following the principle
	 * "Favor Composition over Inheritance"
	 * 
	 * @return the JComponent to be displayed
	 */
	JComponent getComponent();

	/**
	 * Mark a UiElement to highlight invalid values
	 */
	void highlight();
	
}

package de.dhbw.vs.jprakt.guigenerator.gruppe2.factory;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.FensterImpl;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.Fenster;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.FensterFactory;

public class FensterFactoryImpl implements FensterFactory{
	
	public  Fenster create(String name)
	{
		return new FensterImpl(name);
		
	}

}

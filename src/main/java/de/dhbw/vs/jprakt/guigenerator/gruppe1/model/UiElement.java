package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

public abstract class UiElement<T> {

	
	private T instanceValue;
	private final Field associatedField;

	public UiElement(T instanceValue, Field associatedField) {
		this.setInstanceValue(instanceValue);
		this.associatedField = associatedField;

	}

	/**
	 * @return Das UI Element darf nicht modifiziert werden können wenn es final
	 *         deklariert worden ist.
	 */
	public boolean isFinal() {
		return Modifier.isFinal(getAssociatedField().getModifiers());
	}

	public List<Annotation> getAnnotations() {
		return Arrays.asList(getAssociatedField().getAnnotations());
	}

	/**
	 * @return das Feld aus dem das UI-Element erzeugt worden ist.
	 */
	public Field getAssociatedField() {
		return associatedField;
	}

	/**
	 * @return null oder eine Instanz des Klasse, aus welchem das jeweilige
	 *         UIElement erstellt worden ist.
	 */
	public T getInstanceValue() {
		return instanceValue;
	}

	public void setInstanceValue(T instance) {
		this.instanceValue = instance;
	}
	
	public String getLabel(){
		return this.getAssociatedField().getName();
	}

	@Override
	public String toString() {
		return "UiElement [instanceValue=" + instanceValue + ", associatedField=" + associatedField + "]";
	}

	

}

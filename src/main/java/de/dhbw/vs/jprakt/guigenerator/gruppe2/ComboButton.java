package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

/**
 * Erweitert den JButton um ButtonListener hinzu zu fügen
 * und um sie zu aktivieren
 */
public class ComboButton extends JButton{
	
	private List<ButtonListener> listeners = new ArrayList<>();
	
	public ComboButton(String title)
	{
		super(title);
	}

	public void addButtonListener(ButtonListener listener)
	{
		listeners.add(listener);
	}
	
	public void activateButton(String id)
	{
		for(ButtonListener listener : listeners)
		{
			listener.onButtonClicked(id);
		}
	}
}

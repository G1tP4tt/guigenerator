package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.util.ArrayList;

@SuppressWarnings("serial")
public class Dataset extends ArrayList<UiElement<?>> {

	private final Object instance;

	@Override
	public String toString() {
		return "Dataset [instance=" + instance + "]\n"+super.toString();
	}

	public Dataset(Object instance) {
		this.instance = instance;
	}

	public Object getInstance() {
		return instance;
	}
	
	
	/**
	 * @param fieldName
	 * @return Returns an UiElement with the given fieldName from this dataset
	 */
	public UiElement<?> get(String fieldName){
		return this.stream().filter(e -> e.getAssociatedField().getName().equals(fieldName)).findAny().get();
	}

}

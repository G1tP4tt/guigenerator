package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.listlistners;

import java.lang.reflect.InvocationTargetException;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Objectlist;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.GuiGenerator;

/**
 * Buttonlistner for Addbottun on Lists - adds another element to list and shows
 * the dialog for changing the attribute values
 * 
 * @author simon
 *
 */
public class AddListner extends ListChangeListner {

	public AddListner(Objectlist objList) {
		super(objList);
	}

	/**
	 * adds new element to list and updates key values to include it in GUI
	 * dropdown
	 * 
	 */
	@Override
	public void onButtonClicked(String id) {
		createAndAddNewElement();
		updateComboBox(objList.getKeyList());

	}

	private void createAndAddNewElement() {
		try {
			Object newElement = objList.generateNewElement();
			GuiGenerator.createView(newElement);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		try {
			objList.updateMap();
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		;
	}

}

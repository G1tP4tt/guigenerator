package de.dhbw.vs.jprakt.guigenerator.gruppe3.listner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.ClassScanner;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Dataset;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.Fenster;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.factory.UiElementFactory;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.SavedListener;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.BindingElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield.DoubleTextfieldBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield.IntegerTextfieldBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.validation.AnnotationValidator;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.validation.Result;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.validation.SingleResult;

/**
 * Listener for save buttons. Saves GUI content to original object via bindings
 * (Classes extending BindingElement) and ClassScanner. If content contains
 * errors the change is not saved and an error message is created.
 * 
 * @author simon
 *
 * @param <T>
 */
@SuppressWarnings("rawtypes")
public class SaveListener<T> implements ActionListener {

	private Dataset dataset;
	private List<BindingElement> bindings;
	private SavedListener<T> listener;
	private Fenster window;

	public SaveListener(List<BindingElement> bindings, Dataset dataset, SavedListener<T> listener, Fenster window) {
		this.bindings = bindings;
		this.dataset = dataset;
		this.listener = listener;
		this.window = window;

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		Result results = checkGUIdataAndHighlight();

		if (results.isValid())
			saveAndClose();
		else
			showErrorMessage(results);
	}

	private Result checkGUIdataAndHighlight() {
		Result results = new Result();

		for (BindingElement uiDataBinding : bindings) {
			results = checkSingleBinding(results, uiDataBinding);
			if (!results.isValid()) {
				uiDataBinding.getRepresentation().highlight();
			}
		}
		return results;
	}

	private void saveAndClose() {
		{
			for (BindingElement uiDataBinding : bindings) {
				uiDataBinding.updateUiElement();
			}
			transformBack();
			listener.saved((T) dataset.getInstance());
			window.closeWindow();
		}
	}

	private void showErrorMessage(Result results) {
		{
			String errorMessage = new String();

			for (SingleResult singleResult : results) {
				if (!singleResult.isValid()) {
					errorMessage += singleResult.message();
					errorMessage += " \n";
				}
			}
			UiElementFactory.createFehlermeldung(errorMessage, "Fehler");
		}
	}

	private Result checkSingleBinding(Result results, BindingElement uiDataBinding) {
		{
			boolean validData = uiDataBinding.guiValueCanBeWrittenBack();

			if (!validData) {
				SingleResult transformErrorMessage = generateErrormessage(uiDataBinding);
				results.add(transformErrorMessage);
			} else { // only if gui data is correct
				Result errors = getAnnotationValidationErrors(uiDataBinding);
				results.addAll(errors);

			}
			return results;

		}
	}

	private void transformBack() {
		try {
			ClassScanner.transformBack(dataset);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException
				| NoSuchMethodException | InstantiationException | InvocationTargetException e1) {
			throw new RuntimeException(e1);
		}
	}

	private <E, R> SingleResult generateErrormessage(BindingElement<E, R> bindingElem) {
		SingleResult result = SingleResult.create();
		result.setIsValid(false);
		if (bindingElem instanceof IntegerTextfieldBinding) {
			result.setMessage("Bitte geben Sie eine Ganzzahl ein:");
		} else if (bindingElem instanceof DoubleTextfieldBinding) {
			result.setMessage("Bitte geben Sie eine Dezimalzahl ein, mit \".\" als Trennzeichen");
		} else {
			result.setMessage("Es ist ein unerwarteter Fehler aufgetreten.");
			throw new RuntimeException();
		}
		return result;

	}

	private Result getAnnotationValidationErrors(BindingElement uiDataBinding) {
		List<Annotation> annotations = uiDataBinding.getUiElement().getAnnotations();
		AnnotationValidator validator = new AnnotationValidator();
		Object guiValue = uiDataBinding.getRepresentation().getValue();

		Result result = validator.validate(annotations, guiValue);

		return result;
	}
}

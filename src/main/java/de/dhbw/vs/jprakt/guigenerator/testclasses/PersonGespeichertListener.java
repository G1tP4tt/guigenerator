package de.dhbw.vs.jprakt.guigenerator.testclasses;

public interface PersonGespeichertListener {
	public void gespeichert(Person changedPerson);
}

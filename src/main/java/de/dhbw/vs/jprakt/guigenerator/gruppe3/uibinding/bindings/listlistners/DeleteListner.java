package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.listlistners;

import java.util.List;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Objectlist;

/**
 * ButtonListner for DeletButton on Lists - deletes given element from List and
 * updates the gui
 * 
 * @author simon
 *
 */
public class DeleteListner extends ListChangeListner {

	public DeleteListner(Objectlist objList) {
		super(objList);
	}

	@Override
	public void onButtonClicked(String id) {
		objList.remove(id);
		updateKeyList();
		
		List<String> newKeyList = objList.getKeyList();
		updateComboBox(newKeyList);
	}

	private void updateKeyList() {
		try {
			objList.updateMap();
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

}

package de.dhbw.vs.jprakt.guigenerator.gruppe3.validation;
/**
 * contains the result of a validation with validity and message
 * @author simon
 *
 */
public class SingleResult {

	private boolean valid;
	private String message;

	public boolean isValid() {
		return valid;
	}

	public static SingleResult create() {
		return new SingleResult();
	}

	public void setIsValid(boolean validationResult) {
		this.valid = validationResult;
		
	}

	public String message() {
		// TODO Auto-generated method stub
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toString() {
		return "[valid: "+isValid()+"; message: \""+message+"\"]";
	}

	
}

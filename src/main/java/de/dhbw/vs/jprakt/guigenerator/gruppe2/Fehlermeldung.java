package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import javax.swing.JOptionPane;

/*
 * Erzeugt eine Fehlermeldung mit mitgegebenem Fehlertext
 */
public class Fehlermeldung extends JOptionPane{
	
	public Fehlermeldung(String errorMessage, String title)
	{
		this.showMessageDialog(null, errorMessage, title, this.INFORMATION_MESSAGE);
	}

}

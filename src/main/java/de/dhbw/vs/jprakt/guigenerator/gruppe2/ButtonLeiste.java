package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

/*
 * Panel mit cancel - und saveButton
 */
public class ButtonLeiste extends JPanel implements UiElementRepresentation<String>{

	private final long serialVersionUID = -204730098763198081L;
	private JButton speichern = new JButton("Speichern");
	private JButton abbrechen = new JButton("Abbrechen");
	
	public ButtonLeiste(ActionListener cancelAction, ActionListener saveAction)
	{
		GridLayout grid = new GridLayout(1, 2);
		grid.setHgap(5);
		this.setLayout(grid);
		speichern.addActionListener(saveAction);
		abbrechen.addActionListener(cancelAction);
		this.add(abbrechen);
		this.add(speichern);
	}

	@Override
	public String getId()
	{
		return Long.toString(serialVersionUID);
	}

	@Override
	public String getValue()
	{
		return null;
	}

	@Override
	public JComponent getComponent()
	{
		return this;
	}

	@Override
	public void highlight()
	{
	}
}

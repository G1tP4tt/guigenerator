package de.dhbw.vs.jprakt.guigenerator.gruppe3.validation;

import java.util.ArrayList;

/**
 * Contains multiple results and their combined state (isValid())
 * 
 * @author simon
 *
 */
public class Result extends ArrayList<SingleResult> {
	public Result() {
		super();

	}

	public boolean isValid() {
		for (SingleResult sresult : this) {
			if (!sresult.isValid()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder resultBuilder = new StringBuilder();
		for (SingleResult result : this) {
			resultBuilder.append(result.toString() + "\n");
		}
		return "{ " + resultBuilder.toString() + "}";
	}
}

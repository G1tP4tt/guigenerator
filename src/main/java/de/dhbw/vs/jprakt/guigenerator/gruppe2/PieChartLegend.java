package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * 
 * This is the documentation of the class PanelLegend. This class will create a
 * JPanel including the legend of the pieChart depending on the input data given
 * in a hashMap.
 * 
 * @author Vera Gögelein
 * @author Janine Salomon
 *
 */

public class PieChartLegend extends JPanel {

	/**
	 * Array with different colors for filling purposes of the pieChart.
	 */
	private static Color[] ColorPies = { Color.yellow, Color.orange, Color.pink, Color.magenta, Color.red,
			Color.lightGray, Color.green, Color.gray, Color.darkGray, Color.cyan, Color.blue, Color.black };

	/**
	 * Deklaration of xpos and ypos of the legend position.
	 */
	private int xpos;
	private int ypos;

	/**
	 * Building of arrayLists.
	 */
	ArrayList<String> strings;
	ArrayList<Number> values;

	/**
	 * Deklaration of the variable height, space and buffer for setting the
	 * position of the legend.
	 */
	private int sizeRect;
	private int space;
	private int buffer;

	/**
	 * Constructur of the class PanelLegend.
	 * 
	 * @param strings
	 *            ArrayList with strings belonging to the values.
	 * @param values
	 *            ArrayList with the values, needed for drawing the pieChart.
	 * @param buffer
	 *            Buffer as distance between the text.
	 * @param height
	 *            Height of the rectangles and the text.
	 * @param space
	 *            Space for distances between small rectangles of the legend and
	 *            the text.
	 */
	public PieChartLegend(ArrayList<String> strings, ArrayList<Number> values, int buffer, int height, int space,
			int heightPanel, int widthPanel) {
		this.strings = strings;
		this.values = values;
		this.sizeRect = height;
		this.space = space;
		this.buffer = buffer;
		this.setSize(widthPanel, heightPanel);
	}

	/**
	 * The method paintComponent is drawing the legend.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Font font = new Font("Helvetica", Font.PLAIN, 10);
		// Small rectangles for the legend
		for (int i = 0; i < strings.size(); i++) {
			g.setColor(ColorPies[i % ColorPies.length]);

			ypos = i * sizeRect + i * buffer +10;
			g.fillRect(xpos, ypos, sizeRect, sizeRect);
			g.setFont(font);
			g.setColor(Color.black);
			g.drawString(strings.get(i) + ": " + values.get(i) + " GE", xpos + space, ypos + (int) (sizeRect * 0.7));
		}
	}

	/**
	 * The method repaintSizing is setting the new size of the JPanel and
	 * initiate a repaint of the new legend.
	 * 
	 * @param buffer
	 *            Buffer as distance between the text.
	 * @param height
	 *            Height of the rectangles and the text.
	 * @param space
	 *            Space for distances between small rectangles of the legend and
	 *            the text.
	 * @param heightPanel
	 *            Height of the JPanel
	 * @param widthPanel
	 *            Width of the JPanel
	 */
	public void repaintSizing(int buffer, int height, int space, int heightPanel, int widthPanel) {
		this.removeAll();
		this.sizeRect = height;
		this.space = space;
		this.buffer = buffer;
		this.setSize(widthPanel, heightPanel);
		repaint();
	}
}

package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

public class BarChartBar extends JPanel implements Runnable {

	private ArrayList<Number> values;
	// values converted to heights
	private int[] heights, workHeights;
	private int space, barwidth;
	private int scalaHeight;

	// Array with different colors for filling purposes of the barChart.
	private static Color[] ColorPies = { Color.yellow, Color.orange, Color.pink, Color.magenta, Color.red,
			Color.lightGray, Color.green, Color.gray, Color.darkGray, Color.cyan, Color.blue, Color.black };

	public BarChartBar(ArrayList<Number> values, Dimension dim, int space, int barWidth, int scalaWidth,
			int scalaHeight, int startPosX, int startPosY) {
		this.values = values;
		this.space = space;
		this.barwidth = barWidth;
		this.scalaHeight = scalaHeight;
		this.setSize(scalaWidth, scalaHeight);
		this.setLocation(startPosX, startPosY);
		// with opaque false not every pixel will be renderd so background will
		// be displayed. this prohibits weird graphic glitches
		this.setOpaque(false);
		heightMaker();
		repaint();
	}

	/**
	 * calculates actual renderingheights from values
	 */
	private void heightMaker() {
		double temp = 0;
		heights = new int[values.size()];
		workHeights = new int[values.size()];
		// searching for biggest value
		for (int i = 0; i < values.size(); i++) {
			if (temp < values.get(i).doubleValue()) {
				temp = values.get(i).doubleValue();
			}
		}
		// calculation factor for heights
		double factor = this.scalaHeight / temp;

		// value * factor equals the height in pixels

		for (int i = 0; i < values.size(); i++) {
			heights[i] = (int) Math.round(values.get(i).doubleValue() * factor);
		}

	}

	/**
	 * Updating needed values and recalculating sizes
	 * 
	 * @param dim
	 * @param space
	 * @param barWidth
	 * @param scalaWidth
	 * @param scalaHeight
	 * @param startPosX
	 * @param startPosY
	 */
	public void updateSizing(Dimension dim, int space, int barWidth, int scalaWidth, int scalaHeight, int startPosX,
			int startPosY) {
		this.space = space;
		this.barwidth = barWidth;
		this.scalaHeight = scalaHeight;
		this.setSize(scalaWidth, scalaHeight);
		this.setLocation(startPosX, startPosY);
		this.setOpaque(false);
		heightMaker();
		repaint();
	}

	/**
	 * Animation
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		boolean change = true; // chage-indicator
		while (change) { // as long as change is happening
			change = false;
			// grow each bar a pixel till height is reached
			for (int i = 0; i < heights.length; i++) {
				if (workHeights[i] < heights[i]) {
					change = true;
					workHeights[i]++;
				}
			}
			// repainting bars with updated values
			repaint();
			try {
				Thread.sleep(10); // pasuing current thread for X milliseconds.
									// This value represents speed of animation
			} catch (Exception ex) {

			}
		}
	}

	/**
	 * Painting bars to height reached in workHeights
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < workHeights.length; i++) {
			g.setColor(ColorPies[i % workHeights.length]);
			int xPos = (i + 1) * space + i * barwidth;
			int yPos = (scalaHeight) - workHeights[i];
			g.fillRect(xPos, yPos, barwidth, workHeights[i]);
		}

	}

}

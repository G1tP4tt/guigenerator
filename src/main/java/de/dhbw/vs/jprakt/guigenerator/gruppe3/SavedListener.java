package de.dhbw.vs.jprakt.guigenerator.gruppe3;

/**
 * Listens for saved (changed) Objects resulting from the framework.
 * 
 * @author simon
 *
 * @param <T>
 *            Class of given object
 */
public interface SavedListener<T> {
	void saved(T savedObject);
}

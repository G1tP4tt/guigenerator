package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;

public class IntegerElement extends UiElement<Integer> {

	public IntegerElement(Integer value, Field associatedField) {
		super(value, associatedField);
	}

}

package de.dhbw.vs.jprakt.guigenerator.gruppe2.api;

import java.awt.event.ActionListener;

public interface Fenster{
	
	String getId();
	
	/**
	 *  Wenn Elemente zum Fenster hinzugefügt werden, wird dessen Höhe angepasst 
	 *  und das Fenster neu gezeichnet
	 */
	void addUiElementRepresentation(UiElementRepresentation component);

	/**
	 * Durchsucht die UiElementRepresentations anhand der Id.
	 * @param string
	 * @return
	 */
	UiElementRepresentation getById(String id);

	/**
	 * Wird aufgerufen nachdem alle Elemente zum Fenster hinzugefügt wurden.
	 * Fügt am Ende noch die Buttonleiste hinzu
	 */
	void makevisible(ActionListener cancelAction, ActionListener saveAction);

	void closeWindow();

}

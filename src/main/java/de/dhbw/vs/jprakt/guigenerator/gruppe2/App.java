package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.Fenster;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	HashMap<String, Number> in = new HashMap<String, Number>();
		in.put("2009", 50000.00);
		in.put("2011", 50000.50);
		in.put("2000", 55000.00);
		in.put("2013", 50500.00);
		in.put("2008", 60000.00);
		
		HashMap<String, Number> pie = new HashMap<String, Number>();
        pie.put("Zinseinnahmen", 50000);
        pie.put("Pachteinnahmen", 50000);
        pie.put("Gehalt", 55000);
        pie.put("Kindergeld", 50500);
        pie.put("Schwarzgeld", 60000.5);
     
    	
    	BarChart dc = new BarChart(in,"Einkommen");
    	PieChart pc = new PieChart(pie, "Einkommen"); 

    	
    	Fenster fenster = new FensterImpl("NonEditTF");
    	TextFeldImpl test = new TextFeldImpl("test", (int)1);
    	test.nonEditableTF();
    	fenster.addUiElementRepresentation(test);
    	//fenster.addUiElementRepresentation(pc);
    	fenster.addUiElementRepresentation(dc);
    	
    	ActionListener cancelAction = new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.out.println("cancel");
				
			}
		};
		
		ActionListener saveAction = new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.out.println("save");
				
			}
		};

		fenster.makevisible(cancelAction, saveAction);

    }
}

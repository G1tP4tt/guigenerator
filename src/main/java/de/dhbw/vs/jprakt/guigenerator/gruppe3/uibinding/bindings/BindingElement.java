package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

/**
 * Represents a coupling between an UiElement and a corresponding representation.
 * It is used to write the values on the gui back into the uiElements.
 *
 * @param <E> Type of UiElement (Gruppe 1)
 * @param <R> Type of Representation (Gruppe 2)
 */
public abstract class BindingElement<E, R> {

	private UiElement<E> uiElement;
	// Representation that shows in the GUI
	private UiElementRepresentation<R> representation;

	//necessary to instantiate different BindingElement instances with ConcreteBindingelementclass.newInstance();
	public BindingElement() {
	}

	/**
	 * Instantiates the representation of a UiElement.
	 * This method must be called after instantiation. Similar to "@PostConstruct"-annotation.
	 * @param uiElement 
	 * @return
	 */
	public BindingElement<E, R> init(UiElement<E> uiElement) {
		this.uiElement = uiElement;
		this.representation = createRepresentation(uiElement);
		return this;
	}

	/**
	 * Creates representations from an UiElement
	 * @param uiElement
	 * @return corresponding represenation
	 */
	protected abstract UiElementRepresentation<R> createRepresentation(UiElement<E> uiElement);

	/**
	 * Is used to check whether the value in the representation (of type R)
	 * can be transformed to a value from type E
	 * @return true if the value can be transformed, false otherwise
	 */
	public abstract boolean guiValueCanBeWrittenBack();

	/**
	 * writes the value from a representation in an UiElement data
	 * @param representation
	 * @param data
	 * @return the UiElement with the value from the representation
	 */
	public abstract UiElement<E> writeBack(UiElementRepresentation<R> representation, UiElement<E> data);
	
	public UiElementRepresentation<R> getRepresentation() {
		return representation;
	}
	
	public UiElement<E> getUiElement() {
		return uiElement;
	}

	public void updateUiElement() {
		uiElement = writeBack(representation, uiElement);
	}
	
}
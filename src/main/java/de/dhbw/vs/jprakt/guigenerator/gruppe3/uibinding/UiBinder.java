package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.DependentObject;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.BarChartElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.EnumElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.DateElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Dataset;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.DoubleElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.IntegerElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Objectlist;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.PieChartElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.StringElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.BarChartBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.BindingElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.DateBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.DependendObjectBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.EnumBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.ListBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.PieChartBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield.DoubleTextfieldBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield.IntegerTextfieldBinding;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield.StringTextfieldBinding;

/**
 * Creates BindingElements from the corresponding UiElements of Gruppe 1.
 * Binding them with their Representations in the GUI. 
 * 
 * 
 */

@SuppressWarnings({ "rawtypes", "unchecked" })
public class UiBinder {

	private static Map<Class<? extends UiElement>, Class<? extends BindingElement>> possibleBindings = createPossibleBindings();

	public static List<BindingElement> create(Dataset dataset) {
		List<BindingElement> list;
		try {
			list = createChecked(dataset);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		return list;
	}

	static List<BindingElement> createChecked(Dataset dataset) throws InstantiationException, IllegalAccessException {
		List<BindingElement> bindings = new ArrayList<>();
		for (UiElement<?> uiElement : dataset) {
			Class<? extends UiElement> elemClass = uiElement.getClass();
			Class<? extends BindingElement> bindingClass = possibleBindings.get(elemClass);
			if (bindingClass == null) {
				throw new BindingNotFoundException(elemClass);
			}
			BindingElement binding = bindingClass.newInstance();
			binding.init(uiElement);
			bindings.add(binding);
		}
		return bindings;
	}

	private static Map<Class<? extends UiElement>, Class<? extends BindingElement>> createPossibleBindings() {
		Map<Class<? extends UiElement>, Class<? extends BindingElement>> map = new HashMap<>();
		map.put(StringElement.class, StringTextfieldBinding.class);
		map.put(DoubleElement.class, DoubleTextfieldBinding.class);
		map.put(IntegerElement.class, IntegerTextfieldBinding.class);
		map.put(DependentObject.class, DependendObjectBinding.class);
		map.put(EnumElement.class, EnumBinding.class);
		map.put(DateElement.class, DateBinding.class);
		map.put(Objectlist.class, ListBinding.class);
		map.put(BarChartElement.class, BarChartBinding.class);
		map.put(PieChartElement.class, PieChartBinding.class);
		return map;
	}

}

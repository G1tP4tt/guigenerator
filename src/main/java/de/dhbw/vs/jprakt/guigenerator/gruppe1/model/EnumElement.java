package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EnumElement extends UiElement<Enum<?>> {

	private List<Enum<?>> values;
	
	@SuppressWarnings("unchecked")
	public EnumElement(Enum<?> instanceValue, Field associatedField) {
		super(instanceValue, associatedField);
		this.values = (List<Enum<?>>) Arrays.asList(associatedField.getType().getEnumConstants());
	}

	public List<Enum<?>> getValues() {
		return values;
	}

	public void setValues(List<Enum<?>> values) {
		this.values = values;
	}
	
	public List<String> getEnumNames(){
		return values.stream().map(e -> e.name()).collect(Collectors.toList());
	}
	
	//sets the selected enum according to the given int
	public void setSelectedEnum(int selectedEnum) {
		String nameOfEnum = values.get(selectedEnum).name();
		this.setSelectedEnum(nameOfEnum);
	}

	//sets the selected enum according to the given name
	@SuppressWarnings("unchecked")
	public void setSelectedEnum(String nameOfSelectedEnum) {
		Class<? extends Enum> enumType = (Class<? extends Enum>) this.getAssociatedField().getType();
		this.setInstanceValue(Enum.valueOf(enumType, nameOfSelectedEnum));
		
	}
}

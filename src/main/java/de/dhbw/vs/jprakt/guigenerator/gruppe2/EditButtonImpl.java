package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

// Einfacher Button, der fürs Layout in einem Panel mit leerem Label als Platzhalter ist
public class EditButtonImpl extends JPanel implements UiElementRepresentation<String>{

	String value;
	JButton editButton;
	JLabel emptyLabel;
	
	public EditButtonImpl(ActionListener open, String name)
	{
		editButton = new JButton(name);
		editButton.addActionListener(open);
		emptyLabel = new JLabel();
		this.setLayout(new GridLayout(1, 2));
		this.add(editButton);
		this.add(emptyLabel);
		value = name;
	}


	@Override
	public String getId()
	{
		// TODO Auto-generated method stub
		return value;
	}


	@Override
	public String getValue()
	{
		// TODO Auto-generated method stub
		return value;
	}


	@Override
	public JComponent getComponent()
	{
		// TODO Auto-generated method stub
		return this;
	}


	@Override
	public void highlight()
	{
		// Hier nicht nötig, da ein Button keine Fehleingabe zulässt
	}
	
}

package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 * This class creats and draws the coordinate system for the barChart.
 *
 */
public class BarChartGraph extends JPanel {

	private int startPosX, startPosY;
	private int scalaHeight;
	private int scalaWidth;
	private int space;
	private int barWidth;
	// List with all descriptions for labels
	private List<String> strings;

	public BarChartGraph(int startPosX, int startPosY, Dimension dim, List<String> strings, int space, int barWidth,
			int scalaWidth, int scalaHeight) {
		this.setSize(dim);
		this.setLayout(null);
		// Same as in BarChartBar
		this.setOpaque(false);
		this.startPosX = startPosX;
		this.startPosY = startPosY;
		this.scalaHeight = scalaHeight;
		this.scalaWidth = scalaWidth;
		this.space = space;
		this.barWidth = barWidth;
		this.strings = strings;
		createLabels();
		repaint();
	}

	/**
	 * cleares the panel and updates values, recalculates everything and draws
	 * the updated graph
	 * 
	 * @param startPosX
	 * @param startPosY
	 * @param dim
	 * @param space
	 * @param barWidth
	 * @param scalaWidth
	 * @param scalaHeight
	 */
	public void updateSizing(int startPosX, int startPosY, Dimension dim, int space, int barWidth, int scalaWidth,
			int scalaHeight) {
		// cleares everything from the panel, especially the labels
		this.removeAll();
		this.setSize(dim);
		this.startPosX = startPosX;
		this.startPosY = startPosY;
		this.scalaHeight = scalaHeight;
		this.scalaWidth = scalaWidth;
		this.space = space;
		this.barWidth = barWidth;
		createLabels();
		repaint();
	}

	/**
	 * paints the axises of the graph
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawLine(startPosX, startPosY, startPosX, startPosY + scalaHeight);
		g.drawLine(startPosX, startPosY + scalaHeight, startPosX + scalaWidth, startPosY + scalaHeight);
	}

	// creates the describing labels of the bars
	public void createLabels() {
		String sign = "";
		JLabel[] labels = new JLabel[strings.size()];
		for (int i = 0; i < strings.size(); i++) {
			sign = strings.get(i);
			labels[i] = new JLabel();
			labels[i].setText(sign);
			// The Location of the Labels is below the coordinate system
			int xpos = (int) (startPosX + ((i + 1) * space) + (i * barWidth) + (0.3 * barWidth));
			int ypos = scalaHeight + startPosY;
			labels[i].setBounds(xpos, ypos, 100, 30);
			this.add(labels[i]);
			// System.out.println(labels[i].getText());
		}
	}
}

package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

/**
 * Binds a DoubleTextfeld to a Textfield in the UI.
 * It uses a regex to check whether a string is also a valid double.
 * @author boelz
 *
 */
public class DoubleTextfieldBinding extends AbstractTextfield<Double>{

	@Override
	public boolean guiValueCanBeWrittenBack() {
		String textfieldValue = getRepresentation().getValue();
		return isValidDouble(textfieldValue);
	}
	
	//scope is package local for tests.
	static boolean isValidDouble(String value) {
		String doubleRegex = "(-?)\\d+\\.?\\d*";
		return value.matches(doubleRegex);
	}
	
	@Override
	public UiElement<Double> writeBack(UiElementRepresentation<String> representation, UiElement<Double> data) {
		if(guiValueCanBeWrittenBack()){
			Double value=Double.parseDouble(getRepresentation().getValue());
			data.setInstanceValue(value);
		}
	
		return data;
	}

}

package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings;

import java.util.Date;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.factory.UiElementFactory;
/**
 * Used to bind a Datefield to a combobox in the UI.
 * @author boelz
 *
 */
public class DateBinding extends BindingElement<Date, Date>{

	@Override
	protected UiElementRepresentation<Date> createRepresentation(UiElement<Date> uiElement) {
		return UiElementFactory.createDateCombo(uiElement.getInstanceValue(), uiElement.getLabel());
	}

	@Override
	public boolean guiValueCanBeWrittenBack() {
		return true;
	}

	@Override
	public UiElement<Date> writeBack(UiElementRepresentation<Date> representation, UiElement<Date> data) {
		data.setInstanceValue(representation.getValue());
		return data;
	}

}

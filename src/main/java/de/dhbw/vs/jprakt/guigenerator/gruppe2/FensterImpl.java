package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.Fenster;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;


public class FensterImpl extends JFrame implements Fenster{

	private JScrollPane scrollPanel;
	private JPanel rootPanel;
	private List<UiElementRepresentation> uiElementRepresentations= new ArrayList<>();
	private int componentHeight;
	private GridBagLayout layout;
	private GridBagConstraints constraints;
	private int componentCount = 0;
	
	
	public FensterImpl(String name)
	{
		this.setMaximumSize(new Dimension(Window.WIDTH -100, Window.HEIGHT ));
		this.rootPanel = new JPanel();
		
		//GridBagLayout, unterschiedliche Elemente unterschiedliche Größe haben können
		layout= new GridBagLayout();
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(2, 2, 2, 2);
		this.rootPanel.setLayout(layout);
		
		// Wenn das Fenster größer als der Bildschirm wird soll es scrollbar werden
		this.scrollPanel = new JScrollPane(rootPanel);
		scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.getContentPane().add(scrollPanel);
		
		componentHeight = 30;
		this.setSize(600, componentHeight);
		this.setTitle(name);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(false);
		
	}
	
	@Override
	public void addUiElementRepresentation(UiElementRepresentation representation)
	{
		defineConstraints(representation);
		uiElementRepresentations.add(representation);
		this.rootPanel.add(representation.getComponent());
		this.setSize(getWidth(), getHeight()+ componentHeight);
		this.revalidate();
	}

	/*
	 * Legt das Layout der einzelnen Elemente fest
	 */
	private void defineConstraints(UiElementRepresentation representation)
	{	
		constraints.gridy = componentCount ++;
		constraints.gridx = 0;
		constraints.weightx = 0.5;
		
		//Charts sollen 8x größer angezeigt werden als andere Elemente.
		if(representation.getClass() == BarChart.class ||
			representation.getClass()== PieChart.class)
		{
    		constraints.weighty = 0.8;	
    		this.setSize(this.getWidth(), this.getHeight() + componentHeight*8);
		}
		
		else
		{
			constraints.weighty = 0.1;	
		}
		layout.setConstraints((Component) representation, constraints);
	}
	
	
	@Override
	public UiElementRepresentation getById(String id) {
		 Optional<UiElementRepresentation> firstWithMatchingName = uiElementRepresentations.stream().filter(uiElem -> uiElem.getId().equals(id)).findFirst();
		return firstWithMatchingName.get();
	}

	
	@Override
	public String getId() {
		return getTitle();
	}
	
	@Override
		public void makevisible(ActionListener cancelAction, ActionListener saveAction) {
			this.addUiElementRepresentation(new ButtonLeiste(cancelAction, saveAction));
	 		this.setVisible(true);
	 	}
	
	@Override
	public void closeWindow()
	{
		this.dispose();
	}


}

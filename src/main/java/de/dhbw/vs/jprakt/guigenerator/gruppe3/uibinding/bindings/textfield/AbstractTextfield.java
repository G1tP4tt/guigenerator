package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield;

import java.lang.reflect.Field;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.factory.UiElementFactory;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.BindingElement;

/**
 * This class binds an UiElement to a textfield in the UI.
 * It is responsible for creating the representation of the textfield.
 * @author boelz
 *
 * @param <E> the type of the value of the UiElement
 */
public abstract class AbstractTextfield<E> extends BindingElement<E, String>{

	/**
	 * Creates a (non-editable) textfield in the UI
	 * and sets its text to the value of the UiElement.
	 */
	@Override
	protected UiElementRepresentation<String> createRepresentation(UiElement<E> uiElement) {
		Field feld = uiElement.getAssociatedField();
		String lableName = feld.getName();
		E textfieldValue = uiElement.getInstanceValue();
		
		UiElementRepresentation<String> textFeld;
		if(uiElement.isFinal()){
			textFeld=UiElementFactory.createUneditableTextFeld(lableName, String.valueOf(textfieldValue));
		}else{
			textFeld = UiElementFactory.createTextFeld(lableName, String.valueOf(textfieldValue));
		}
		return textFeld;
	}
	
	
}

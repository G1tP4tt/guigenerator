package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.BorderLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

/**
 * 
 * This is the documentation of the class barChart. This class will create a
 * barChart depending on the input data given in a hashMap.
 * 
 * @author Vera Gögelein
 * @author Janine Salomon
 * @author Sebastian Loew
 *
 */
public class BarChart extends JPanel implements ComponentListener, UiElementRepresentation<HashMap<String, Number> > {
	/**
	 * Saves the amount of elements in the hashMap.
	 */
	private int amount;
	/**
	 * Saves the maximum value of the values array.
	 */
	private int maxValue = 0;
	/**
	 * Buffer between the maximum value and the end of the scala.
	 */
	private int buffer;
	/**
	 * Saves height and width of the scala, including buffer.
	 */
	private int totalHeight, totalWidth;
	/**
	 * The height of the scala is is 80% of the height of the panel.
	 */
	private int heightScala;
	/**
	 * The width of the scala is 80% of the width of the panel.
	 */
	private int widthScala;
	/**
	 * The space between the bars is 30px.
	 */
	private int space;
	/**
	 * Scaling width of a bar in the chart depending on the amount of data in
	 * the given hashMap.
	 */
	private int barWidth;
	/** 
	 * Variable sets the start position depending on the size of the panel.
	 */
	private int startPosX, startPosY;
	
	private String id;
	/**
	 * The ArrayList strings is data type String and saves the years of the
	 * member values.
	 */
	private ArrayList<String> strings = new ArrayList<String>();
	/**
	 * The ArrayList values is data type Number and saves the values of the
	 * member years.
	 */
	private ArrayList<Number> values = new ArrayList<Number>();
	
	
	private HashMap<String, Number> input;
	
	//Graphpanel as Background
	private BarChartGraph bcg;
	
	// Barpanel with animation 
	private BarChartBar bcb;
	/**
	 * Constructor of the class barChart. The barChart is created depending on
	 * the given data in the hashMap. There is a check if the hashMap is empty.
	 * If the hashMap is empty there will be a JLabel with a annotation. If the
	 * hashMap is filled the values are sorted into an arrayList.
	 * 
	 * @param in
	 *            HashMap
	 */
	public BarChart(HashMap<String, Number> in, String id) {
		//adding component listener to react on resizing
		this.addComponentListener(this);
		this.input = in;
		this.id = id;
		this.setLayout(new BorderLayout());
		setSizing();
		// Absorption of an empty hashMap.
		if (in.isEmpty()) {
			JLabel ausgabe = new JLabel();
			ausgabe.setText("Es liegen keine Daten zur Erstellung eines BarChart vor!");
			ausgabe.setBounds(startPosX, startPosY, heightScala, widthScala);
			this.add(ausgabe);
		} else {
			amount = in.size();
			// Sorting of the hashMap according to strings(ascending)
			Map<String, Number> map = new TreeMap<String, Number>(in);
			Set set = map.entrySet();
			Iterator iterator = set.iterator();
			// Adding of the data types String and Number to the member
			// ArrayList
			while (iterator.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator.next();
				strings.add((String) entry.getKey());
				values.add((Number) entry.getValue());
			}
			// Determine MaxValue
			for (int i = 0; i < values.size(); i++) {
				if (maxValue < values.get(i).intValue()) {
					maxValue = values.get(i).intValue();
				}
			}
			drawBars();
			//Adding LayerdPane to stack panes
			JLayeredPane layer = new JLayeredPane();
			layer.setSize(this.getSize().width, this.getSize().height);
			layer.setBorder(BorderFactory.createTitledBorder(this.id + ":"));
			// initialising graph and bars and adding them to layerdPane
			bcg = new BarChartGraph(startPosX, startPosY, this.getSize(), strings, space, barWidth, widthScala, heightScala);
			bcb = new BarChartBar(values, this.getSize(), space, barWidth, widthScala, heightScala, startPosX, startPosY);
			layer.add(bcg);
			layer.add(bcb);
			this.add(layer);
			// starting animation
			new Thread(bcb).start();
		}
	}
	
	public void setSizing() {
		//Depending on the panel size all values are calculated.
		totalHeight = this.getHeight();
		totalWidth = this.getWidth();
		buffer = (int) Math.round(this.getWidth() * 0.1);
		heightScala = (int) Math.round(this.getHeight() * 0.8);
		widthScala = (int) Math.round(this.getWidth() * 0.8);
		space = (int) Math.round(this.getWidth() * 0.05);
		startPosX = (int) Math.round(this.getWidth() * 0.08);
		startPosY = (int) Math.round(this.getHeight() * 0.08);
	}
	

	/**
	 * This method calculates barWidth for the drawing of the bars.
	 */
	public void drawBars() {
		barWidth = (widthScala / amount) - space;
		repaint();
	}

	@Override
	public String getId() {
		return this.id; 
	}

	@Override
	public HashMap<String, Number> getValue() {
		return this.input;
	}

	@Override
	public JComponent getComponent() {
		return this;
	}

	
	@Override
	public void highlight() {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * on resize recalculating and rerendering chart
	 * @param e
	 */
	@Override
	public void componentResized(ComponentEvent e) {
		//update sizing
		setSizing();
		drawBars();
		// refresh values
		bcg.updateSizing(startPosX, startPosY, this.getSize(), space, barWidth, widthScala, heightScala);
		bcb.updateSizing(this.getSize(), space, barWidth, widthScala, heightScala, startPosX, startPosY);
		repaint();
		//starting repainting
		new Thread(bcb).start();
	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

}
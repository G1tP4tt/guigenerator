package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

/**
 * Binds a StringTextfeld to a Textfeld in the UI.
 * @author boelz
 *
 */
public class StringTextfieldBinding extends AbstractTextfield<String>{

	public StringTextfieldBinding() {
		super();
	}

	@Override
	public boolean guiValueCanBeWrittenBack() {
		return true;
	}

	@Override
	public UiElement<String> writeBack(UiElementRepresentation<String> representation, UiElement<String> data) {
		String guiValue = representation.getValue();
		data.setInstanceValue(guiValue);
		return data;	
	}

	
}
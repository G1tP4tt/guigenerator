package de.dhbw.vs.jprakt.guigenerator.gruppe3;

import java.util.List;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.ClassScanner;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Dataset;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.Fenster;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.listner.CancelListner;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.listner.SaveListener;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.UiBinder;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.WindowCreator;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.BindingElement;

/**
 * Entry class of the GUI-Generating-Framework. This class generates a GUI for a
 * given Object.
 * 
 * @author simon
 *
 */
public class GuiGenerator  {

	/**
	 * analyses given Object o and creates a GUI for it. in the GUI the values
	 * contained in o can be modified. the resulting modified object can be used
	 * in the listner object where the saved-method is called, after save was
	 * clicked in the root GUI Object representing o.
	 * 
	 * @param o
	 *            the object that will be displayed and modified
	 * @param listener
	 *            the listner to access the resulting object
	 */
	public static <T> void createView(T o, SavedListener<T> listener) {
		Dataset dataset = null;
		dataset = createDatasetFromObject(o, dataset);
		createViewWithDataset(dataset, listener);
	}

	private static Dataset createDatasetFromObject(Object o, Dataset dataset) {
		try {
			dataset = ClassScanner.createDataset(o);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		return dataset;
	}
	
	private static <T> void createViewWithDataset(Dataset datensatz, SavedListener<T> externalSaveListner) {

		WindowCreator fErzeuger = new WindowCreator();
		Fenster fenster = fErzeuger.create(datensatz);
		List<BindingElement> bindings = UiBinder.create(datensatz);
		for (BindingElement uiDataBinding : bindings) {
			fenster.addUiElementRepresentation(uiDataBinding.getRepresentation());
		}

		fenster.makevisible(new CancelListner(fenster),
				new SaveListener(bindings, datensatz, externalSaveListner, fenster));
	}

	public static void createView(Object o) {
		createView(o, (person) -> {
			System.out.println(o.toString());
		});
	}

	

}

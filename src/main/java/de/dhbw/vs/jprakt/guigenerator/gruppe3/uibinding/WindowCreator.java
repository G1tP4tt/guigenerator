package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Dataset;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.Fenster;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.FensterFactory;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.factory.FensterFactoryImpl;

/**
 * creates a window (instance from Fenster interface) using Gruppe2s factory.
 * The window can be used for showing UIRepresentations.
 * 
 * @author simon
 *
 */
public class WindowCreator {

	public Fenster create(Dataset dataset) {
		FensterFactory windowFactory = new FensterFactoryImpl();
		Fenster window = windowFactory.create(dataset.getInstance().getClass().getSimpleName());
		return window;
	}

}

package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.listlistners;

import java.util.List;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Objectlist;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.ButtonListener;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.ObjectCombo;

/**
 * Superclass for List-modification ButtonListner implementations
 * 
 * @author simon
 *
 */
public abstract class ListChangeListner implements ButtonListener {

	protected Objectlist objList;
	private ObjectCombo uiRepresentation;

	public ListChangeListner(Objectlist objList) {
		this.objList = objList;
	}
	 void updateComboBox(List<String> ids){
		getUiRepresentation().updateComboBox(ids);
	}
	public ObjectCombo getUiRepresentation() {
		return uiRepresentation;
	}
	public void setUiRepresentation(ObjectCombo uiRepresentation) {
		this.uiRepresentation = uiRepresentation;
	}
}
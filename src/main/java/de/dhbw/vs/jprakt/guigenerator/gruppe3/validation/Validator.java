package de.dhbw.vs.jprakt.guigenerator.gruppe3.validation;

/**
 * contains the rules for the validation of objects
 * 
 * @author simon
 *
 */
public class Validator {

	public boolean notNull(Object testableObject) {
		if (testableObject == null) {
			return false;
		}
		return true;
	}

	public boolean matchesPattern(Object testableObject, String regex) {
		if (notNull(testableObject)) {
			if (testableObject instanceof String) {
				String testableString = (String) testableObject;
				// TODO check if Ferdi agrees with uppercasing
				Boolean erg = testableString.toUpperCase().matches(regex);

				return erg;
			}
		}
		return false;
	}

	public boolean sizeIsBetween(Object testableObject, int minsize, int maxsize) {

		if (notNull(testableObject)) {
			int length = testableObject.toString().length();

			if (length >= minsize && length <= maxsize) {
				return true;
			}
		}
		return false;
	}

}

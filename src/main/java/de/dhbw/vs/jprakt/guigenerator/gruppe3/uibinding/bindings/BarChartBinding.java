package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings;

import java.util.Map;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.factory.UiElementFactory;

public class BarChartBinding<T extends Number> extends BindingElement<Map<String, T>, Map<String, T>> {
	@Override
	protected UiElementRepresentation<Map<String, T>> createRepresentation(
			UiElement<Map<String,T>> uiElement) {
		
			return UiElementFactory.createBarChart(uiElement.getInstanceValue(), uiElement.getLabel());
	}

	@Override
	public boolean guiValueCanBeWrittenBack() {
		return true;
	}


	@Override
	public UiElement<Map<String, T>> writeBack(UiElementRepresentation<Map<String, T>> representation,
			UiElement<Map<String, T>> data) {
		return data;
	}
}

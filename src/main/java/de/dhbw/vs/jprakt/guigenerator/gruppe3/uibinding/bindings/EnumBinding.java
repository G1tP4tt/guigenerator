package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.EnumElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.factory.UiElementFactory;

/**
 * Binds Enums to Dropdown menus in the User Interface.
 * 
 * @param <E> the type of the enum
 */
public class EnumBinding<E> extends BindingElement<E, Integer> {


	/**
	 * Creates the enum combobox (=dropdown) form the UiElement.
	 * the possible enum values are stored in a list of strings.
	 * the selected enum is indicated by the position in this list.
	 */
	@Override
	protected UiElementRepresentation<Integer> createRepresentation(UiElement uiElement) {
		Enum instanceValue = (Enum) uiElement.getInstanceValue();
		
		String selected = instanceValue.name();
		Class<? extends Enum> e = instanceValue.getClass();
		Stream<String> enumStream = Arrays.stream(e.getEnumConstants()).map(Enum::name);
		List<String> enumNames = enumStream.collect(Collectors.toList());
		int position = findPos(selected, enumNames);
		return UiElementFactory.createEnumCombo(enumNames, position, ((EnumElement)uiElement).getLabel());
	}

	private int findPos(String selected, List<String> values) {
		for (int i = 0; i < values.size(); i++) {
			String string = values.get(i);
			if (string.equals(selected)) {
				return i;
			}
		}
		throw new RuntimeException("Enumwert nicht Teil des Ursprungsenums [unmöglich]");
	}

	@Override
	public boolean guiValueCanBeWrittenBack() {
		return true;
	}

	/**
	 * sets the instanceValue of the UiElement to the selected enum.
	 * The position in the dropdown is used to determine which enum is currently selected.
	 */
	@Override
	public UiElement writeBack(UiElementRepresentation representation, UiElement data) {
		Integer value = (Integer) representation.getValue();
		EnumElement theCB = (EnumElement) data;
		theCB.setSelectedEnum(value);
		
		return data;
	}

}

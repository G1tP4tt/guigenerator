package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;
import java.util.Map;

public class BarChartElement extends UiElement<Map<String, ? extends Number>> {

	public BarChartElement(Map<String, ? extends Number> instanceValue, Field zugehörigesFeld) {
		super(instanceValue, zugehörigesFeld);
	}

}

package de.dhbw.vs.jprakt.guigenerator.gruppe3;

import de.dhbw.vs.jprakt.guigenerator.testclasses.ShowcasePreface;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		

		//official test:
		GuiGenerator.createView(ShowcasePreface.test(), obj->System.out.println(obj));
	}

}

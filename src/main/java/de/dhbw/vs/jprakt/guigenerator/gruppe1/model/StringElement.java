package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;

public class StringElement extends UiElement<String> {

	public StringElement(String value, Field associatedField) {
		super(value, associatedField);
	}
}

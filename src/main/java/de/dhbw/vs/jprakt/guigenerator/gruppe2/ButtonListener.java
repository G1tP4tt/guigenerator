package de.dhbw.vs.jprakt.guigenerator.gruppe2;

/**
 * Listener um beim Klicken auf einen der 3 Buttons bei der
 * Objekt-ComboBox zu reagieren und den ausgewählten Wert der 
 * ComboBox mit zu geben
 */
public interface ButtonListener{
	
	void onButtonClicked(String id);

}

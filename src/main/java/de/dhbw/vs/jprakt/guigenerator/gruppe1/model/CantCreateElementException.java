package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

@SuppressWarnings("serial")
public class CantCreateElementException extends RuntimeException {

	public CantCreateElementException(String s) {
		super(s);	
	}
}

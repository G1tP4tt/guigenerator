package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.textfield;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;


/**
 * Binds a IntegerTextfeld to a Textfield in the UI.
 * It uses a regex to check whether a string is also a valid double.
 * @author boelz
 *
 */
public class IntegerTextfieldBinding extends AbstractTextfield<Integer> {

	@Override
	public boolean guiValueCanBeWrittenBack() {
		String textfieldValue = getRepresentation().getValue();

		return isValidInteger(textfieldValue);
	}
	
	//scope is package local for tests.
	static boolean isValidInteger(String string){
		String intRegex = "(-?)\\d+";
		return string.matches(intRegex);
	}

	@Override
	public UiElement<Integer> writeBack(UiElementRepresentation<String> representation, UiElement<Integer> data) {
		if (guiValueCanBeWrittenBack()) {
			Integer value = Integer.parseInt(getRepresentation().getValue());
			data.setInstanceValue(value);
		}

		return data;
	}

}

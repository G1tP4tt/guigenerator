package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

public class DateCombo extends JPanel implements UiElementRepresentation<Date>, FocusListener {
	
	private JLabel id;
	private JComboBox<Integer> daySel;
	private JComboBox<String> monthSel;
	private JComboBox<Integer> yearSel;
	
	private int day;
	private int month;
	private int year;
	
	private Color dayBackround;
	private Color monthBackround;
	private Color yearBackround;
	
	public DateCombo(Date selection, String id) {
		this.id = new JLabel(id);
		decompileDate(selection);
		
		//initialising comboboxes with dates and months and years
		addDay();
		addMonth();
		addYear();
		
		//keep default background
		dayBackround = daySel.getBackground();
		monthBackround = monthSel.getBackground();
		yearBackround = yearSel.getBackground();
		
		//preselecting given date
		daySel.setSelectedIndex(this.day - 1);
		monthSel.setSelectedIndex(this.month - 1);
		yearSel.setSelectedIndex(this.year - 1970);
		
		//Focuslistener for changing backgroundlistener
		addFocusListener();
		
		addComponent();
	}

	/**
	 * Adding focuslistener to comboboxes
	 */
	private void addFocusListener()
	{
		daySel.addFocusListener(this);
		monthSel.addFocusListener(this);
		yearSel.addFocusListener(this);
	}

	
	@Override
	public String getId() {
		return this.id.getText();
	}

	@Override
	public Date getValue() {
		Date result = new Date();
		// getting date set in comboboxes
		this.day = daySel.getSelectedIndex() + 1;
		this.month = monthSel.getSelectedIndex() + 1;
		this.year = yearSel.getSelectedIndex() + 1970;
		
		Calendar cal = Calendar.getInstance();
		// with Lenient false date will not calculate to a future date and throw exception when wrong
		cal.setLenient(false);
		//when date makes no sense will throw exception and highlight will be called
		try {
			cal.set(this.year, this.month - 1, this.day);
			result = cal.getTime();
		} catch (Exception ex) {
			highlight();
		}
		
		return result;
	}

	@Override
	public JComponent getComponent() {
		return this;
	}

	/**
	 * When Error occures background will be set red to show error
	 */
	@Override
	public void highlight() {
		// TODO Auto-generated method stub
		this.daySel.setBackground(Color.red);
		this.monthSel.setBackground(Color.red);
		this.yearSel.setBackground(Color.red);
		
	}
	
	/**
	 * Adding Comboboxes and Label to Panel with GridLayout(1,4)
	 */
	private void addComponent() {
		this.setLayout(new GridLayout(1, 4));
		this.add(this.id);
		this.add(this.daySel);
		this.add(this.monthSel);
		this.add(this.yearSel);
	}
	
	/**
	 * Sets day, month and year val from date
	 * @param datum
	 */
	private void decompileDate(Date datum) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(datum);
		this.day = cal.get(Calendar.DAY_OF_MONTH);
		this.month = cal.get(Calendar.MONTH) + 1;
		this.year = cal.get(Calendar.YEAR);
	}
	
	private void addDay() {
		daySel = new JComboBox<Integer>();
		for(int i = 1; i <= 31; i++) {
			daySel.addItem(i);
		}
	}
	
	private void addMonth() {
		monthSel = new JComboBox<String>();
		monthSel.addItem("Januar");
		monthSel.addItem("Februar");
		monthSel.addItem("März");
		monthSel.addItem("April");
		monthSel.addItem("Mai");
		monthSel.addItem("Juni");
		monthSel.addItem("Juli");
		monthSel.addItem("August");
		monthSel.addItem("September");
		monthSel.addItem("Oktober");
		monthSel.addItem("November");
		monthSel.addItem("Dezember");
	}
	
	private void addYear() {
		yearSel = new JComboBox<Integer>();
		for(int i = 1970; i <= 2050; i++) {
			yearSel.addItem(i);
		}
	}
	
	// When Comboboxes focused highlighting will be removed
	public void removeHighlighting()
	{
		this.daySel.setBackground(dayBackround);
		this.monthSel.setBackground(monthBackround);
		this.yearSel.setBackground(yearBackround);
	}

	@Override
	public void focusGained(FocusEvent e)
	{
		removeHighlighting();
	}
	
	@Override
	public void focusLost(FocusEvent e)
	{
		
	}

}

package de.dhbw.vs.jprakt.guigenerator.gruppe3.validation;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Validates Annotations for a Object with a given Validator. returns a fitting
 * result with message and evaluation-result - if the annotation was a
 * validation-annotation.
 * 
 * @author simon
 *
 */
public class AnnotationValidator {

	private Validator validator;

	public AnnotationValidator() {
		this.validator = new Validator();
	}

	public AnnotationValidator(Validator validator) {
		this.validator = validator;
	}

	public Optional<SingleResult> validate(Annotation annotation, Object toValidate) {
		SingleResult result;

		if (annotation instanceof NotNull)
			result = validateNotNull(annotation, toValidate);
		else if (annotation instanceof Pattern)
			result = validatePattern(annotation, toValidate);
		else if (annotation instanceof Size)
			result = validateSize(annotation, toValidate);
		else {
			return Optional.empty();
		}

		return Optional.of(result);

	}

	private SingleResult validateNotNull(Annotation annotation, Object toValidate) {
		SingleResult result;
		{
			result = SingleResult.create();
			NotNull notNull = (NotNull) annotation;
			boolean validationResult = validator.notNull(toValidate);
			result.setIsValid(validationResult);
			if ("{javax.validation.constraints.NotNull.message}".equals(notNull.message())) {
				result.setMessage("Ein auszufuellendes Feld wurde nicht ausgefüllt");
			} else {
				result.setMessage(notNull.message());
			}
		}
		return result;
	}

	private SingleResult validatePattern(Annotation annotation, Object toValidate) {
		SingleResult result;
		{
			result = SingleResult.create();
			Pattern pattern = (Pattern) annotation;
			String regexp = pattern.regexp();
			boolean validationResult = validator.matchesPattern(toValidate, regexp);
			result.setIsValid(validationResult);
			if ("{javax.validation.constraints.Pattern.message}".equals(pattern.message())) {
				result.setMessage("Ein Pattern stimmt nicht überein");
			} else {
				result.setMessage(pattern.message());
			}
		}
		return result;
	}

	private SingleResult validateSize(Annotation annotation, Object toValidate) {
		SingleResult result;
		{
			result = SingleResult.create();
			Size size = (Size) annotation;
			int min = size.min();
			int max = size.max();
			boolean validationResult = validator.sizeIsBetween(toValidate, min, max);
			result.setIsValid(validationResult);
			if ("{javax.validation.constraints.Size.message}".equals(size.message())) {
				result.setMessage("Eine Groessenanforderung wurde nicht erfuellt");
			} else {
				result.setMessage(size.message());
			}
		}
		return result;
	}

	public Result validate(List<Annotation> annotations, Object toValidate) {
		Result results = new Result();
		for (Annotation annotation : annotations) {
			Optional<SingleResult> optionalResult = validate(annotation, toValidate);
			if (optionalResult.isPresent()) {
				results.add(optionalResult.get());
			}
		}
		return results;
	}

}

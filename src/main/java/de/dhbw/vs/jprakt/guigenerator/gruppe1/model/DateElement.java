package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;
import java.util.Date;

public class DateElement extends UiElement<Date>{

    public DateElement(Date instanceValue, Field associatedField) {
        super(instanceValue, associatedField);
    }

}

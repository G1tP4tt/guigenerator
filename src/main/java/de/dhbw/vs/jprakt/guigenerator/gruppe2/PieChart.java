package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

/**
 * 
 * This is the documentation of the class pieChart. This class will create a
 * pieChart depending on the input data given in a hashMap.
 * 
 * @author Vera Gögelein
 * @author Janine Salomon
 *
 */
public class PieChart extends JPanel implements ComponentListener, UiElementRepresentation<HashMap<String, Number>> {
	/**
	 * The ArrayList strings is data type String and saves the type of income of
	 * the member values.
	 */
	ArrayList<String> strings = new ArrayList<String>();
	/**
	 * The ArrayList values is data type Number and saves the values of the
	 * member type of income.
	 */
	ArrayList<Number> values = new ArrayList<Number>();
	/**
	 * Declaration and initialization of variables for distances between parts
	 * of the legend.
	 */
	private int buffer;
	private int space;
	private int height;

	/**
	 * Deklaration of the variable input as HashMap input for further purposes.
	 */
	private HashMap<String, Number> input;

	/**
	 * Deklaration of the variable id as String input for further purposes.
	 */
	private String id;

	/**
	 * Deklaration of variables for setting the position of the JLabel if the
	 * given hashMap is empty.
	 */
	private int startEmpty;
	private int widthEmpty;
	private int heightEmpty;

	/**
	 * Deklaration of the objects PanelPie (JPanel with the pieChart) and
	 * PanelLegend (JPanel with the legend).
	 */
	private PieChartPie piePanel;
	private PieChartLegend legendPanel;

	/**
	 * Deklaration of variables for setting the size of the JPanel including the
	 * pieChart (PanelPie).
	 */
	private int heightPieChartPie;
	private int widthPieChartPie;

	/**
	 * Deklaration of variables for setting the size of the JPanel including the
	 * legend of the pieChart (PanelLegend).
	 */
	private int heightPieChartLegend;
	private int widthPieChartLegend;

	/**
	 * 
	 * Constructor of the class pieChart. The pieChart is created depending on
	 * the given data in the hashMap. There is a check if the hashMap is empty.
	 * If the hashMap is empty there will be a JLabel with a annotation. If the
	 * hashMap is filled the values are sorted into an arrayList.
	 * 
	 * @param in
	 *            HashMap
	 * @param id
	 *            id
	 */
	public PieChart(HashMap<String, Number> in, String id) {
		this.addComponentListener(this);
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.input = in;
		this.id = id;
		// Initial size of the JPanel.
		this.setSize(300, 400);
		this.calcSizes();
		// Absorption of an empty hashMap.
		if (in.isEmpty()) {
			JLabel ausgabe = new JLabel();
			ausgabe.setText("Es liegen keine Daten zur Erstellung eines PieChart vor!");
			ausgabe.setBounds(startEmpty, startEmpty, widthEmpty, heightEmpty);
			this.add(ausgabe);
		}
		// working on a filled hashMap.
		else {
			// Sorting of the hashMap according to strings(ascending)
			Map<String, Number> map = new TreeMap<String, Number>(in);
			Set set = map.entrySet();
			Iterator iterator = set.iterator();
			// Adding of the data types String and Number to the member
			// ArrayList
			while (iterator.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator.next();
				strings.add((String) entry.getKey());
				values.add((Number) entry.getValue());
			}
			panelBorder();

			// Building of a PanelPie for drawing the pieChart in.
			piePanel = new PieChartPie(values, heightPieChartPie, widthPieChartPie);
			// Starting of thread for rotation.
			new Thread(piePanel).start();

			// Building of a PanelLegend for drawing the legend in.
			legendPanel = new PieChartLegend(strings, values, buffer, height, space, heightPieChartLegend,
					widthPieChartLegend);

			this.add(piePanel);
			this.add(legendPanel);
		}
	}

	/**
	 * The method calcSizes calculates the values depending on the panel size.
	 */
	public void calcSizes() {
		buffer = (int) Math.round(this.getHeight() * 0.01);
		space = (int) Math.round(this.getHeight() * 0.08);
		height = (int) Math.round(this.getHeight() * 0.05);
		startEmpty = (int) Math.round(this.getWidth() * 0.05);
		widthEmpty = this.getWidth();
		heightEmpty = (int) Math.round(this.getHeight() * 0.05);
		heightPieChartLegend = this.getHeight();
		widthPieChartLegend = (int) Math.round(this.getWidth() * 0.5);
		heightPieChartPie = (int) Math.round(this.getHeight() * 0.9);
		widthPieChartPie = (int) Math.round(this.getWidth() * 0.5);
	}

	/**
	 * The method panelBorder sets a border to the pieChart.
	 */
	public void panelBorder() {
		this.setBorder(BorderFactory.createTitledBorder(id + ": "));
	}

	/**
	 * The method comonentResized is calling if the JFrame is resized. If the
	 * JFrame is resized all values are calculated new.
	 */
	@Override
	public void componentResized(ComponentEvent e) {
		calcSizes();
		// Setting of new sizes for the panels legendPanel (PanelLegend) and
		// piePanel (PanelPie).
		try
		{
			Thread.sleep(10);
		} catch (InterruptedException e1)
		{
			e1.printStackTrace();
		}
		legendPanel.repaintSizing(buffer, height, space, heightPieChartLegend, widthPieChartLegend);
		piePanel.repaintSizing(heightPieChartPie, widthPieChartPie);

		// Starting of thread for rotation.
		new Thread(piePanel).start();
	}

	@Override
	public void componentMoved(ComponentEvent e) {
	}

	@Override
	public void componentShown(ComponentEvent e) {

	}

	@Override
	public void componentHidden(ComponentEvent e) {

	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public HashMap<String, Number> getValue() {
		return this.input;
	}

	@Override
	public JComponent getComponent() {
		return this;
	}

	@Override
	public void highlight() {

	}
}
package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.listlistners;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Objectlist;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.GuiGenerator;

/**
 * ButtonListner for EditButton on Lists - creates new window for changing the
 * object for a given id
 * 
 * @author simon
 *
 */
public class EditListner extends ListChangeListner {

	public EditListner(Objectlist objList) {
		super(objList);
	}

	@Override
	public void onButtonClicked(String id) {
		Object object = objList.get(id);
		GuiGenerator.createView(object,updatedObject->{
			updateKeyList();
			updateComboBox(objList.getKeyList());}); // changes original Object -> no
			
											// back-saving necessary only id update in gui
		
	}

	private void updateKeyList() {
		try {
			objList.updateMap();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}

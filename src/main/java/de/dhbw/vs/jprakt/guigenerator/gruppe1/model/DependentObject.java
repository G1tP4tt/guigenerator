package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class DependentObject<T> extends UiElement<T> {

	protected Dataset data;

	public DependentObject(T InstanceValue, Field associatedField)
			throws IllegalArgumentException, IllegalAccessException {
		super(InstanceValue, associatedField);

	}

	public boolean hasDataset() {
		return this.data != null;
	}

	public Dataset getDataset() throws NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		this.initData();
		return data;
	}

	public void initData() throws NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (this.getInstanceValue() == null) {
			Class<?> clazz = getAssociatedField().getType();
			Constructor<?> ctor = clazz.getConstructor();
			ctor.setAccessible(true);

			@SuppressWarnings("unchecked")
			T object = (T) ctor.newInstance();

			ctor.setAccessible(false);
			this.setInstanceValue(object);
			this.initData();

		} else {
			data = ClassScanner.createDataset(this.getInstanceValue());
		}
	}

	@Override
	public String toString() {
		return "DependentObject [instanceValue=" + getInstanceValue() + ",\n\t data=" + (data != null
				? data.toString().replaceAll("\n", "") : data) + "]";
	}

}

package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings;

import java.util.List;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.Objectlist;
import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.ObjectCombo;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.factory.UiElementFactory;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.listlistners.AddListner;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.listlistners.DeleteListner;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings.listlistners.EditListner;

/**
 * Creates a selector for elements of given list for modifying the list and the
 * list elements.
 * 
 * @author simon
 *
 */
public class ListBinding extends BindingElement<List<?>, String> {

	@Override
	protected UiElementRepresentation<String> createRepresentation(UiElement<List<?>> uiElement) {

		Objectlist objList = (Objectlist) uiElement;
		EditListner editListener = new EditListner(objList);
		DeleteListner removeListener = new DeleteListner(objList);
		AddListner addListener = new AddListner(objList);
		UiElementRepresentation<String> uiRepresentation = UiElementFactory.createObjectCombo(objList.getKeyList(),
				uiElement.getLabel(), editListener, addListener, removeListener);
		// Add GUI Element to listners for later updating
		ObjectCombo objCombo = (ObjectCombo) uiRepresentation;
		editListener.setUiRepresentation(objCombo);
		addListener.setUiRepresentation(objCombo);
		removeListener.setUiRepresentation(objCombo);
		return uiRepresentation;
	}

	@Override
	public boolean guiValueCanBeWrittenBack() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public UiElement<List<?>> writeBack(UiElementRepresentation<String> representation, UiElement<List<?>> data) {
		// TODO Auto-generated method stub
		return data;
	}

}

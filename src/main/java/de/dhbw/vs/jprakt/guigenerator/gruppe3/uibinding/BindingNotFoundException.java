package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
/**
 * Indicates that there is no Mapping for a 
 * UiElement to its corresponding (Gui-)Representation.
 * The binding is missing in this case.
 * @author boelz
 *
 */
public class BindingNotFoundException extends RuntimeException{

	public BindingNotFoundException(Class<? extends UiElement> elemClass) {
		super("Der Klasse "+elemClass.getName()+" fehlt ein UiDataBinding!");
	}
}

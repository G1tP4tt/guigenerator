package de.dhbw.vs.jprakt.guigenerator.gruppe3.uibinding.bindings;

import java.lang.reflect.Field;

import de.dhbw.vs.jprakt.guigenerator.gruppe1.model.UiElement;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.factory.UiElementFactory;
import de.dhbw.vs.jprakt.guigenerator.gruppe3.GuiGenerator;
/**
 * Binds a Abhängiges Objekt to a EditButton
 * @author boelz
 *
 * @param <E>
 * @param <R>
 */
public class DependendObjectBinding<E, R> extends BindingElement<E, R>{

	public DependendObjectBinding() {
		super();
	}

	/**
	 * Creates the representation in form of a Button.
	 * If the button is clicked, another window is opened that contains 
	 * the attributes of the Dependent Object.
	 */
	@Override
	protected  UiElementRepresentation createRepresentation(UiElement<E> uiElement) {
		Field feld = uiElement.getAssociatedField();
		//Lambda + funktionales Interface
		return UiElementFactory.createEditButton((event)->{
			GuiGenerator.createView(uiElement.getInstanceValue());
		}, feld.getName()+" ändern");
	}

	/**
	 * Returns always true because the validation of attributes of the dependent object 
	 * happens in the separately created window.
	 */
	@Override
	public boolean guiValueCanBeWrittenBack() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public UiElement<E> writeBack(UiElementRepresentation<R> representation, UiElement<E> data) {
		// TODO Auto-generated method stub
		return data;
	}

	
	
	
	
}
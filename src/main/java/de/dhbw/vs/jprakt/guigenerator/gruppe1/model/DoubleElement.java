package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;

public class DoubleElement extends UiElement<Double> {

	public DoubleElement(Double value, Field associatedField) {
		super(value,  associatedField);
	}

}

package de.dhbw.vs.jprakt.guigenerator.gruppe3.listner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.Fenster;

/**
 * ActionListner for closing the given Window without saving
 * 
 * @author simon
 *
 */
public class CancelListner implements ActionListener {

	private Fenster window;

	public CancelListner(Fenster window) {
		this.window = window;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		window.closeWindow();
	}

}

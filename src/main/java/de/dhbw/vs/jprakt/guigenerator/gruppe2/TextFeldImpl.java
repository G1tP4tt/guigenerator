package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

/**
 * create a panel with a textfield and a label.
 */
@SuppressWarnings("serial")
public class TextFeldImpl extends JPanel implements UiElementRepresentation<String>, FocusListener{

	private String name;
	private String value;

	private JTextField textField;
	private JLabel label;
	private Border tfBorder;

	//create a constructor for a name and value as string textfield
	public TextFeldImpl(String name, String value) {
		this.name = name;
		this.value = value;

		addContent();
		addComponents();

	}
	//create a constructor for a name as string and value as double textfield
	public TextFeldImpl(String name, double value) {
		this.value = String.valueOf(value);
		this.name = name;
		addContent();
		addComponents();

	}
	
	//create a constructor for a name as string and value as integer 
	public TextFeldImpl(String name, Integer value) {
		this.value = String.valueOf(value);
		this.name = name;

		addContent();
		addComponents();
		
	}
	
	// add textfield, Label and Focuslistener 
	public void addContent() {
		this.textField = new JTextField(value);
		this.textField.addFocusListener(this);
		this.label = new JLabel(name);

	}
	
	// create a method for a non editable textfield 
	public void nonEditableTF() {
		textField.setEditable(false);
	}
	
	//create a GridLayout for a label and a textfield to the JPanel
	private void addComponents() {
		this.setLayout(new GridLayout(1, 2));
		this.add(label);
		this.add(textField);
	}

	@Override
	public String getId() {
		return this.name;
	}

	@Override
	public String getValue() {

		return this.textField.getText();
	}


	@Override
	public JComponent getComponent() {
		return this;
	}

	
	// create method for highlithing the textfield at wrong entry
	@Override
	public void highlight()
	{
		textField.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
	}

	// create method for remove highlithing at right entry
	public void removeHighlithing()
	{
		if(tfBorder == null)
		{
			tfBorder = textField.getBorder();
		}
		
		textField.setBorder(tfBorder);
	}

	// execute the method removeHighlithing after clicked the textfield.
	@Override
	public void focusGained(FocusEvent e)
	{
		removeHighlithing();
	}

	@Override
	public void focusLost(FocusEvent e)
	{
	}

}
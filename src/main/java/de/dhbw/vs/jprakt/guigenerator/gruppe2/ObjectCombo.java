package de.dhbw.vs.jprakt.guigenerator.gruppe2;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

public class ObjectCombo extends JPanel implements UiElementRepresentation<String>, ActionListener, ItemListener {
	
	private JComboBox<?> idComboBox;
	private String title;
	private ComboButton editBtn;
	private ComboButton addBtn;
	private ComboButton removeBtn;
    private String value;
    
	public ObjectCombo(List<String> ids, String title, ButtonListener editListener, ButtonListener addListener, ButtonListener removeListener)
	{
		idComboBox = new JComboBox<>(ids.toArray());
		
		this.value = idComboBox.getSelectedItem().toString();
		this.title = title;
		
		editBtn = new ComboButton("edit");
		addBtn = new ComboButton("add");
		removeBtn = new ComboButton("remove");
		
		addListener(editListener, addListener, removeListener);
		
		addComponents();
	}


	private void addListener(ButtonListener editListener, ButtonListener addListener, ButtonListener removeListener)
	{
		editBtn.addButtonListener(editListener);
		addBtn.addButtonListener(addListener);
		removeBtn.addButtonListener(removeListener);
		
		editBtn.addActionListener(this);
		addBtn.addActionListener(this);
		removeBtn.addActionListener(this);
		
		idComboBox.addItemListener(this);
	}


	private void addComponents()
	{
		GridLayout grid = new GridLayout(1, 0);
		grid.setHgap(5);
		this.setLayout(grid);
		this.add(idComboBox);
		this.add(editBtn);
		this.add(addBtn);
		this.add(removeBtn);
		this.setBorder(BorderFactory.createTitledBorder(title));
	}

	public void updateComboBox(List<String> ids)
	{
		idComboBox.setModel(new DefaultComboBoxModel(ids.toArray()));
		setValue(idComboBox.getSelectedItem().toString());
	}

	@Override
	public String getId()
	{
		return title;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	@Override
	public String getValue()
	{
		return value;
	}


	@Override
	public JComponent getComponent()
	{
		return this;
	}


	@Override
	public void highlight()
	{
	}

	/**
	 * Beim klick auf einen der Buttons soll der ButtonListener aktiviert werden 
	 * und der selektierte Wert mitgegeben werden.
	 * Damit kann Gruppe 3 auf den Wert zugreifen.
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource().getClass() == ComboButton.class)
		{
			((ComboButton) e.getSource()).activateButton(value);
		}
	}

	// Wenn ein anderer Wert ausgewählt wird, wird value neu gesetzt
	@Override
	public void itemStateChanged(ItemEvent e)
	{
		setValue(idComboBox.getSelectedItem().toString());
	}

}

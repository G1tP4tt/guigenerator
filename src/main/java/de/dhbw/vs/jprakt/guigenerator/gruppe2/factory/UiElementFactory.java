package de.dhbw.vs.jprakt.guigenerator.gruppe2.factory;

import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import de.dhbw.vs.jprakt.guigenerator.gruppe2.BarChart;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.ButtonListener;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.DateCombo;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.EditButtonImpl;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.EnumCombo;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.Fehlermeldung;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.ObjectCombo;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.PieChart;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.TextFeldImpl;
import de.dhbw.vs.jprakt.guigenerator.gruppe2.api.UiElementRepresentation;

/*
 * Factroy zur Erstellung aller UiElemente
 */
public class UiElementFactory {

	public static UiElementRepresentation<String> createTextFeld(String name, String value) {
		return new TextFeldImpl(name, value);
	}
	
	public static UiElementRepresentation<Integer> createEnumCombo(List<String> content, int selection, String id){
		return new EnumCombo(content, selection, id);
	}
	
	public static UiElementRepresentation<Date> createDateCombo(Date activeDate, String id){
		return new DateCombo(activeDate, id);
	}

	public static UiElementRepresentation<?> createEditButton(ActionListener open, String name) {
		return new EditButtonImpl(open, name);
	}

	public static UiElementRepresentation<String> createUneditableTextFeld(String lableName, String textfieldValue){
		// TODO Auto-generated method stub
		TextFeldImpl textField = new TextFeldImpl(lableName, textfieldValue);
		textField.nonEditableTF();
		return textField;
	}

	public static UiElementRepresentation<String> createObjectCombo(List<String> ids, String title, ButtonListener editListener, ButtonListener addListener, ButtonListener removeListener )
	{
		return new ObjectCombo(ids, title, editListener, addListener, removeListener);
		
	}
	
	
	public static UiElementRepresentation createPieChart(Map<String, ? extends Number>input, String title)
	{
		
		HashMap<String, Number> map = new HashMap(input);
		return new PieChart(map, title);
	}
	
	public static UiElementRepresentation createBarChart(Map<String, ? extends Number> input, String name)
	{
		HashMap<String, Number> map = new HashMap(input);
		return new BarChart(map, name);
	}
	
	public static JOptionPane createFehlermeldung(String errorMessage, String title)
	{
		return new Fehlermeldung(errorMessage, title);
	}
}
